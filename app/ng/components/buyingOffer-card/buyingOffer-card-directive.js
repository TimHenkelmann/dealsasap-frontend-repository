'use strict';

angular.module('myApp.buyingOffers')

    .directive('mvBuyingOfferCard', function() {
        return {
            restrict: 'A',
            scope: {
                buyingOffer: '='
            },
            templateUrl: 'components/buyingOffer-card/buyingOffer-card.html'
        };
    });
