'use strict';

angular.module('myApp.buyingOffers')

    .factory('BuyingOffer', function($resource) {
        return $resource('http://localhost:3000/api/buyingOffers/:buyingOfferId', {buyingOfferId: '@_id'});

    });