'use strict';

angular.module('myApp.buyingOffers')

    .constant('buyingOfferDetailsState', {
        name: 'buyingOffers.detail',
        options: {
            url: '/{buyingOfferId}',

            views: {
                "content@root": {
                    templateUrl: 'views/detail/buyingOffer-detail.html',
                    controller: 'BuyingOfferDetailCtrl'
                }
            },

            resolve: {
                //we abuse the resolve feature for eventual redirection
                redirect: function($state, $stateParams, BuyingOffer, $timeout, $q){
                    var mid = $stateParams.buyingOfferId;
                    if (!mid) {
                        //timeout because the transition cannot happen from here
                        $timeout(function(){
                            $state.go("buyingOffers.list");
                        });
                        return $q.reject();
                    }
                }
            },
            ncyBreadcrumb: {
                // a bit ugly (and not stable), but ncybreadcrumbs doesn't support direct access
                // to a view controller yet if there are multiple views
                label: "{{$$childHead.$$childHead.buyingOffer.title}}",
                parent: "buyingOffers.list"
            }

        }
    })
    .controller('BuyingOfferDetailCtrl', function($scope, BuyingOffer, $mdToast, $mdDialog, $stateParams, $state, currUser) {

        $scope.buyingOffer = BuyingOffer.get({buyingOfferId: $stateParams.buyingOfferId});

        $scope.mayDelete;
        $scope.mayEdit = currUser.loggedIn();
        $scope.deleteBuyingOffer = deleteBuyingOffer;
        $scope.updateBuyingOffer = updateBuyingOffer;
        $scope.cancelEditingBuyingOffer = function(){ showSimpleToast("Editing cancelled"); }

        $scope.buyingOffer.$promise.then(function(){
            $scope.mayDelete = $scope.buyingOffer.user && $scope.buyingOffer.user == currUser.getUser()._id;
        });

        $scope.$watch(function(){
            return currUser.loggedIn();
        }, function(loggedIn){
            if (!loggedIn) {
                $scope.mayDelete = false;
                $scope.mayEdit = false;
            } else {
                $scope.mayEdit = true;
                $scope.mayDelete = $scope.buyingOffer.user == currUser.getUser()._id;
            }
        });

        ////////////////////


        function updateBuyingOffer(changed) {

            if (!changed) {
                showSimpleToast("no change");
                return;
            }

            $scope.buyingOffer.$update().then(function(updated){
                $scope.buyingOffer = updated;
                showSimpleToast("update successfull");
            }, function(){
                showSimpleToast("error. please try again later");
            });
        }

        function deleteBuyingOffer(ev) {

            var confirm = $mdDialog.confirm()
                .title('Are you sure you want to delete this buying offer?')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('Abort');

            var toastText;
            $mdDialog.show(confirm).then(function() {
                return $scope.buyingOffer.$remove().then(function() {
                    return $state.go('buyingOffers.list');
                }).then(function(){
                    showSimpleToast('Buying offer deleted successfully');
                }, function() {
                    showSimpleToast("Error. Try again later");
                });
            }, function() {
                showSimpleToast("delete aborted");
            })
        }

        function showSimpleToast(txt) {
            $mdToast.show(
                $mdToast.simple()
                    .textContent(txt)
                    .position('bottom right')
                    .hideDelay(3000)
            );
        }


    });