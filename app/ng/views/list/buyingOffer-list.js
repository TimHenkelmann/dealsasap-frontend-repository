'use strict';

angular.module('myApp.buyingOffers')

    .constant('buyingOfferListState', {
        name: 'buyingOffers.list',
        options: {

            // Using an empty url means that this child state will become active
            // when its parent's url is navigated to. Urls of child states are
            // automatically appended to the urls of their parent. So this state's
            // url is '/buyingOffers' (because '/buyingOffers' + '').
            url: '',

            // IMPORTANT: Now we have a state that is not a top level state. Its
            // template will be inserted into the ui-view within this state's
            // parent's template; so the ui-view within contacts.html. This is the
            // most important thing to remember about templates.
            views: {
                'content@root': {
                    templateUrl: 'views/list/buyingOffer-list.html',
                    controller: 'BuyingOfferListCtrl',
                },
                'outside@root': {
                    templateUrl: 'views/list/buyingOffer-list-buttons.html',
                    controller: 'buyingOfferListButtonCtrl'
                }
            },

            ncyBreadcrumb: {
                label: "BuyingOffer"
            }

        }

    })

    .controller('BuyingOfferListCtrl', function($scope, BuyingOffer) {
        $scope.buyingOffers = BuyingOffer.query();

        $scope.$on('buyingOfferCreated', function(ev, buyingOffer){
            $scope.buyingOffers.push(buyingOffer);
        });


    })

    .controller('buyingOfferListButtonCtrl', function($scope, $mdMedia, $mdDialog, $mdToast, currUser){

        $scope.createBuyingOfferDialog = createBuyingOfferDialog;
        $scope.authed = false;

        $scope.$watch(function(){
            return currUser.loggedIn();
        }, function(loggedIn){
            $scope.authed = loggedIn;
        });

        ////////////////////////////////////

        function createBuyingOfferDialog(ev) {
            var useFullScreen = ( $mdMedia('xs'));
            $mdDialog.show({
                    controller: "CreateBuyingOfferCtrl",
                    templateUrl: 'components/create-buyingOffer/create-buyingOffer.html',
                    targetEvent: ev,
                    clickOutsideToClose:true,
                    fullscreen: useFullScreen,
                    preserveScope:true
                })
                .then(function(answer) {

                    if (answer) {
                        showSimpleToast('Buying Offer saved successfully');
                    } else {
                        showSimpleToast('An Error occured!');
                    }
                }, function() {
                    showSimpleToast('Buying Offer creation cancelled');
                });

        }

        function showSimpleToast(txt){
            $mdToast.show(
                $mdToast.simple()
                    .textContent(txt)
                    .position('bottom right')
                    .hideDelay(3000)

            );
        }
    });